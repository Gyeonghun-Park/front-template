import { render as renderReact } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { ReactElement } from "react";

export const render = async (component: ReactElement, options = {}) => {
  const user = userEvent.setup();

  return {
    user,
    ...renderReact(component),
  };
};
