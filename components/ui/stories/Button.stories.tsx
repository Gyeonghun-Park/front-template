import type { Meta, StoryObj } from "@storybook/react";

import { Button } from "@/components/ui/button";

const meta = {
  title: "Example/Button",
  component: Button,
  argTypes: {
    variant: {
      description: "The variant of the button",
    },
  },
  parameters: {
    layout: "centered",
    docs: {
      description: {
        component: "This is a button",
      },
    },
  },
  tags: ["autodocs"],
} satisfies Meta<typeof Button>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
  args: {
    variant: "default",
  },
};

export const Warning: Story = {
  args: {
    variant: "destructive",
  },
};

export const Secondary: Story = {
  args: {
    variant: "secondary",
  },
};

export const Large: Story = {
  args: {
    size: "lg",
    variant: "default",
  },
};

export const Small: Story = {
  args: {
    size: "sm",
    variant: "default",
  },
};
