import { screen } from "@testing-library/react";

import { mockSidebarStore } from "@/vitest/mockZustandStore";
import { render } from "@/vitest/render";

import Page from "../page";

it("should renders the 'Hello World' button", async () => {
  const onExpandFn = vi.fn();

  mockSidebarStore({ collapsed: true, onExpand: onExpandFn });
  const { user } = await render(<Page />);

  expect(screen.getByText("collapsed")).toBeInTheDocument();

  const collapesBtn = screen.getByRole("button", { name: "collapes" });
  await user.click(collapesBtn);
  expect(screen.getByText("collapsed")).toBeInTheDocument();

  const expandBtn = screen.getByRole("button", { name: "expand" });
  await user.click(expandBtn);
  expect(onExpandFn).toBeCalledTimes(1);
});
