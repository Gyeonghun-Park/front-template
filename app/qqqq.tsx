"use client";

import { Button } from "@/components/ui/button";
import { useSidebar } from "@/store/use-sidebar";

export const QQQ = () => {
  const { onCollapse, onExpand } = useSidebar("onCollapse", "onExpand");

  return (
    <div>
      <Button onClick={onCollapse}>collapes!!</Button>
      <Button onClick={onExpand}>expand!!</Button>
    </div>
  );
};
