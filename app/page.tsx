import { unstable_noStore } from "next/cache";

import { ScrollArea } from "@/components/ui/scroll-area";

const tags = Array.from({ length: 50 }).map(
  (_, i, a) => `v1.2.0-beta.${a.length - i}`,
);

const Home = async () => {
  unstable_noStore();
  const res = await fetch(
    "http://www.randomnumberapi.com/api/v1.0/random?min=100&max=1000&count=5",
  );
  const data = await res.json();

  return (
    <main className="flex flex-col">
      <header className="h-screen flex-shrink-0">{data}</header>

      <div className="grid h-full flex-1 grid-cols-2 overflow-clip ">
        <ScrollArea className="h-full overscroll-contain bg-red-200">
          <div className="p-4">
            <h4 className="mb-4 text-sm font-medium leading-none">Tags</h4>
            {tags.map((tag) => (
              <>
                <div key={tag} className="text-sm">
                  {tag}
                </div>
              </>
            ))}
          </div>
        </ScrollArea>
        <ScrollArea className="h-full bg-blue-200">
          <div className="p-4">
            <h4 className="mb-4 text-sm font-medium leading-none">Tags</h4>
            {tags.map((tag) => (
              <>
                <div key={tag} className="text-sm">
                  {tag}
                </div>
              </>
            ))}
          </div>
        </ScrollArea>
      </div>
    </main>
  );
};

export default Home;
