import { shallow } from "zustand/shallow";
import { createWithEqualityFn } from "zustand/traditional";

import { createStoreHook } from "@/lib/store";

const sidebarState = { collapsed: false };
export type SidebarState = typeof sidebarState;
export type SidebarStore = SidebarState & {
  onExpand: () => void;
  onCollapse: () => void;
};

export const sidebarStore = createWithEqualityFn<SidebarStore>(
  (set) => ({
    ...sidebarState,
    onExpand: () => set(() => ({ collapsed: false })),
    onCollapse: () => set(() => ({ collapsed: true })),
  }),
  shallow,
);
export const useSidebar = createStoreHook(sidebarStore);
