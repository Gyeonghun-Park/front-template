"use client";

import { Button } from "@/components/ui/button";
import { useSidebar } from "@/store/use-sidebar";

export const Sidebar = () => {
  const { collapsed, onCollapse, onExpand } = useSidebar(
    "collapsed",
    "onCollapse",
    "onExpand",
  );

  return (
    <div>
      <Button onClick={onCollapse}>collapes</Button>
      <Button onClick={onExpand}>expand</Button>
      <div>{collapsed ? "collapsed" : "expanded"}</div>
    </div>
  );
};
