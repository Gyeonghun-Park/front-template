import { type SidebarStore, sidebarstore } from "@/store/use-sidebar";

const mockStore = (hook: any, state: any) => {
  const initStore = hook.getState();
  hook.setState({ ...initStore, ...state }, true);
};

export const mockSidebarStore = (state: Partial<SidebarStore>) => {
  mockStore(sidebarstore, state);
};
